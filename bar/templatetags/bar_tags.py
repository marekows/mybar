from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter(name='cut')
def cut(value, arg):
    """Removes all values of arg from the given string"""
    return value.replace(arg, ' ')


@register.filter
@stringfilter
def dash_to_space(value):
    return value.replace('-', ' ')