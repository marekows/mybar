from django.contrib import admin
from .models import Drink, Ingredient, Profile

admin.site.register([Drink, Ingredient, Profile])
# Register your models here.
