from django.urls import path, include
from lazysignup.views import convert
from .views import all_drinks, user_drinks, ingredients, common

urlpatterns = [
    path('', common.main_redirect),
    path('about/', common.about, name='about'),
    path('my-drinks/', include([
        path('all', user_drinks.all_list, name='user_drinks'),
        path('with/<int:pk>', user_drinks.by_ingredient_list,
             name='user_drinks_by_ingredient'),
        path('favorites', user_drinks.favorites_list,
             name='user_favorites_drinks'),
        path('recommended', user_drinks.recommended_list,
             name='recommended_user_drinks'),
        path('hidden', user_drinks.hidden_list, name='hidden_user_drinks'),
        path('hidden_drink/add/<int:pk>',
             user_drinks.add_hidden_drink, name='add_hidden_drink'),
        path('hidden_drink/remove/<int:pk>',
             user_drinks.remove_hidden_drink, name='remove_hidden_drink'),
        path('favorite_drink/add/<int:pk>',
             user_drinks.add_favorite_drink, name='add_favorite_drink'),
        path('favorite_drink/remove/<int:pk>',
             user_drinks.remove_favorite_drink, name='remove_favorite_drink'),
    ])),
    path('all-drinks/', include([
        path('', all_drinks.all_list, name='all_drinks'),
        path('recommended', all_drinks.recommended_list,
             name='recommended_drinks'),
        path('with/<int:pk>', all_drinks.by_ingredient_list,
             name='drinks_by_ingredient'),
        path('drink_details/<int:pk>', all_drinks.drink_details,
             name='drink_details'),
    ])),
    path('ingredients/', include([
        path('', ingredients.list_all, name='ingredients'),
        path('add/<int:pk>',
             ingredients.add_ingredient, name='add_ingredient'),
        path('remove/<int:pk>',
             ingredients.remove_ingredient, name='remove_ingredient'),
    ])),
    path('switch_hidden_drink/<int:pk>',
         user_drinks.switch_hidden_drink, name='switch_hidden_drink'),
    path('switch_favorite_drink/<int:pk>',
         user_drinks.switch_favorite_drink, name='switch_favorite_drink'),
    path('convert', convert, name='lazysignup_convert'),
    path('ingredients/', ingredients.list_all, name='lazysignup_convert_done')
]
