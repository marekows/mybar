from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.postgres.fields import ArrayField


class Ingredient(models.Model):

    name = models.CharField(max_length=50)
    top = models.BooleanField(default=False)
    hierarchy = models.IntegerField(
        default=1, validators=[MinValueValidator(1), MaxValueValidator(3)])

    class Meta:
        ordering = ('-top', 'hierarchy',)

    def __str__(self):
        return self.name


class Drink(models.Model):
    name = models.CharField(max_length=50)
    ingredients = models.ManyToManyField(Ingredient)
    short_recipe = models.TextField(blank=True)
    full_recipe = models.TextField(blank=True)
    top = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        super(Drink, self).save(*args, **kwargs)
        Profile.objects.filter(is_calculated=True).update(is_calculated=False)

    def __str__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    ingredients = models.ManyToManyField(Ingredient, blank=True)
    hidden_drinks = models.ManyToManyField(
        Drink, blank=True, related_name='profile_hidden_drinks')
    favorite_drinks = models.ManyToManyField(
        Drink, blank=True, related_name='profile_favorite_drinks')
    possible_drinks = ArrayField(
        models.IntegerField(), blank=True, default=list)
    is_calculated = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.user.username} Profile'
