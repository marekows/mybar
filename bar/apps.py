from django.apps import AppConfig


class BarConfig(AppConfig):
    name = 'bar'

    def ready(self):
        import bar.signals
