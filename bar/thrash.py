from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView
from bar.models import Drink, Ingredient, Profile
from django.http import HttpResponse, HttpResponseRedirect
from django.http import HttpResponse
from lazysignup.decorators import allow_lazy_user
from django.contrib import messages
from functools import wraps


@allow_lazy_user
def ingredients(request):

    user = request.user

    user_ingredients = user.profile.ingredients.all()
    all_ingredients = Ingredient.objects.all()

    ingredients_category_1 = [
        element for element in all_ingredients if element.hierarchy == 1]
    ingredients_category_2 = [
        element for element in all_ingredients if element.hierarchy == 2]
    ingredients_category_3 = [
        element for element in all_ingredients if element.hierarchy == 3]

    context = {'user_ingredients': user_ingredients, 'ingredients_category_1': ingredients_category_1,
               'ingredients_category_2': ingredients_category_2, 'ingredients_category_3': ingredients_category_3}

    return render(request, 'bar/ingredients.html', context)


def about(request):

    return render(request, 'bar/about.html')


@allow_lazy_user
def all_drinks(request):

    user = request.user
    all_drinks = Drink.objects.all()
    user_hidden_drinks = user.profile.hidden_drinks.all()
    user_favorites_drinks = user.profile.favorite_drinks.all()
    drinks = all_drinks.exclude(id__in=user_hidden_drinks)
    all_ingredients = Ingredient.objects.all()

    title = 'All Drinks'
    context = {'title': title, 'drinks': drinks, 'user_ingredients': all_ingredients,
               'user_favorites_drinks': user_favorites_drinks, 'user_hidden_drinks': user_hidden_drinks}

    return render(request, 'bar/all_drinks.html', context)


@allow_lazy_user
def recommended_drinks(request):
    user = request.user
    all_ingredients = Ingredient.objects.all()
    drinks = Drink.objects.filter(top=True)
    user_hidden_drinks = user.profile.hidden_drinks.all()
    user_favorites_drinks = user.profile.favorite_drinks.all()

    title = 'Recommended Drinks'

    context = {'title': title, 'drinks': drinks, 'user_ingredients': all_ingredients,
               'user_favorites_drinks': user_favorites_drinks, 'user_hidden_drinks': user_hidden_drinks}

    return render(request, 'bar/all_drinks.html', context)


@allow_lazy_user
def drinks_by_ingredient(request, pk):
    user = request.user
    all_ingredients = Ingredient.objects.all()
    user_hidden_drinks = user.profile.hidden_drinks.all()
    user_favorites_drinks = user.profile.favorite_drinks.all()
    filter_ingredient = all_ingredients.get(pk=pk)
    drinks = Drink.objects.all().exclude(
        id__in=user_hidden_drinks).filter(ingredients=filter_ingredient)
    title = 'Drinks with %s' % filter_ingredient.name.title()

    context = {'title': title, 'drinks': drinks, 'user_hidden_drinks': user_hidden_drinks,
               'user_favorites_drinks': user_favorites_drinks, 'user_ingredients': all_ingredients}

    return render(request, 'bar/all_drinks.html', context)


@allow_lazy_user
def user_drinks(request):
    profile = request.user.profile
    user_ingredients = profile.ingredients.all()
    user_favorites_drinks = profile.favorite_drinks.all()
    user_hidden_drinks = profile.hidden_drinks.all()
    all_ingredients = Ingredient.objects.all()

    if not profile.is_calculated:
        drinks = calculate_drinks(profile, user_ingredients)
    else:
        list_drinks = profile.possible_drinks
        drinks = Drink.objects.filter(
            id__in=list_drinks)

    title = 'All Possible Drinks'
    context = {'title': title, 'drinks': drinks, 'user_ingredients': user_ingredients,
               'user_favorites_drinks': user_favorites_drinks, 'user_hidden_drinks': user_hidden_drinks}

    return render(request, 'bar/user_drinks.html', context)


def calculate_drinks(profile, user_ingredients):

    all_ingredients = Ingredient.objects.all()
    all_drinks = Drink.objects.all()
    user_drinks = all_drinks.exclude(
        ingredients__in=all_ingredients.exclude(id__in=user_ingredients))
    user_hidden_drinks = profile.hidden_drinks.all()
    drinks = user_drinks.exclude(id__in=user_hidden_drinks)

    old_list_drinks = drinks.values_list('pk', flat=True)
    list_drinks = list(old_list_drinks)

    profile.possible_drinks = list_drinks
    profile.is_calculated = True
    profile.save()

    return drinks

@allow_lazy_user
def recommended_user_drinks(request):
    user = request.user
    all_ingredients = Ingredient.objects.all()
    all_recommended_drinks = Drink.objects.filter(top=True)
    user_ingredients = user.profile.ingredients.all()
    user_hidden_drinks = user.profile.hidden_drinks.all()
    user_favorites_drinks = user.profile.favorite_drinks.all()
    recommended_user_drinks = all_recommended_drinks.exclude(
        ingredients__in=all_ingredients.exclude(id__in=user_ingredients))
    drinks = recommended_user_drinks.exclude(id__in=user_hidden_drinks)
    title = 'Recommended Possible Drinks'

    context = {'title': title, 'drinks': drinks, 'user_ingredients': user_ingredients,
               'user_favorites_drinks': user_favorites_drinks, 'user_hidden_drinks': user_hidden_drinks}

    return render(request, 'bar/user_drinks.html', context)


@allow_lazy_user
def user_hidden_drinks(request):
    user = request.user
    user_ingredients = user.profile.ingredients.all()
    user_hidden_drinks = user.profile.hidden_drinks.all()
    title = 'Hidden Drinks'
    hidden = True

    context = {'title': title, 'hidden': hidden,
               'drinks': user_hidden_drinks, 'user_ingredients': user_ingredients}
    return render(request, 'bar/user_drinks.html', context)


@allow_lazy_user
def user_favorites_drinks(request):
    user = request.user
    user_favorites_drinks = user.profile.favorite_drinks.all()
    user_ingredients = user.profile.ingredients.all()
    user_hidden_drinks = user.profile.hidden_drinks.all()
    no_user_ingredients = Ingredient.objects.exclude(id__in=user_ingredients)
    no_user_drinks = Drink.objects.filter(ingredients__in=no_user_ingredients)
    title = 'Favorites Drinks'

    context = {'title': title, 'drinks': user_favorites_drinks, 'user_favorites_drinks': user_favorites_drinks,
               'user_hidden_drinks': user_hidden_drinks, 'no_user_drinks': no_user_drinks,
               'no_user_ingredients': no_user_ingredients, 'user_ingredients': user_ingredients}

    return render(request, 'bar/user_drinks.html', context)


@allow_lazy_user
def user_drinks_by_ingredient(request, pk):
    user = request.user
    all_ingredients = Ingredient.objects.all()
    all_drinks = Drink.objects.all()
    user_ingredients = user.profile.ingredients.all()
    user_hidden_drinks = user.profile.hidden_drinks.all()
    user_favorites_drinks = user.profile.favorite_drinks.all()
    filter_ingredient = all_ingredients.get(pk=pk)
    all_user_drinks = all_drinks.exclude(ingredients__in=all_ingredients.exclude(
        id__in=user_ingredients).exclude(id__in=user_hidden_drinks))
    drinks = all_user_drinks.exclude(id__in=user_hidden_drinks)
    drinks = drinks.filter(ingredients=filter_ingredient)
    title = 'Possible Drinks with %s' % filter_ingredient.name.title()

    context = {'title': title, 'drinks': drinks, 'user_hidden_drinks': user_hidden_drinks, 'user_ingredients': user_ingredients,
               'user_favorites_drinks': user_favorites_drinks}

    return render(request, 'bar/user_drinks.html', context)


@allow_lazy_user
def add_ingredient(request, pk):
    user = request.user
    ingredient = get_object_or_404(Ingredient, pk=pk)
    user.profile.ingredients.add(ingredient)
    user.profile.is_calculated = False
    user.profile.save()

    messages.success(request, '%s - added' % ingredient.name.title())
    return redirect(ingredients)


@allow_lazy_user
def remove_ingredient(request, pk):
    user = request.user
    ingredient = get_object_or_404(Ingredient, pk=pk)
    user.profile.ingredients.remove(ingredient)
    user.profile.is_calculated = False
    user.profile.save()

    messages.success(request, '%s - removed' % ingredient.name.title())
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@allow_lazy_user
def add_hidden_drink(request, pk, ):
    user = request.user
    drink = get_object_or_404(Drink, pk=pk)
    user.profile.hidden_drinks.add(drink)
    user.profile.is_calculated = False
    user.profile.save()

    # return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/#3'))
    messages.success(request, '%s - hidden' % drink.name.title())
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@allow_lazy_user
def remove_hidden_drink(request, pk):
    user = request.user
    drink = get_object_or_404(Drink, pk=pk)
    user.profile.hidden_drinks.remove(drink)
    user.profile.is_calculated = False
    user.profile.save()

    messages.success(request, '%s - unhidden' % drink.name.title())
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@allow_lazy_user
def add_favorite_drink(request, pk):
    user = request.user
    drink = get_object_or_404(Drink, pk=pk)
    user.profile.favorite_drinks.add(drink)
    user.profile.save()

    # return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/#3'))
    messages.success(request, '%s - added to favorites' % drink.name.title())
    # return redirect('/#l3')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@allow_lazy_user
def remove_favorite_drink(request, pk):
    user = request.user
    drink = get_object_or_404(Drink, pk=pk)
    user.profile.favorite_drinks.remove(drink)
    user.profile.save()
    messages.success(request, '%s removed from favorites' % drink.name.title())
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

'''
    #print(Profile.objects.annotate(num_ingredients=Count('ingredients')).filter(num_ingredients__lt=3))
    queryset1 = Profile.objects.annotate(num_ingredients=Count('ingredients')).filter(num_ingredients__gt=25)
    for profile in queryset1:
        user = profile.user
        print(is_lazy_user(user))
'''
    '''
@user_data
def user_drinks(request, user_ingredients, user, all_drinks, all_ingredients, f_ing, user_hidden_drinks):

  user_top_ingredients = user_ingredients.filter(top = True)
  user_no_top_ingredients = user_ingredients.filter(top = False)
  user_favorites_drinks = user.profile.favorite_drinks.all()
  no_user_ingredients = all_ingredients.exclude(id__in=user_ingredients)
  all_user_drinks = all_drinks.exclude(ingredients__in=all_ingredients.exclude(id__in=user_ingredients))
  drinks = all_user_drinks.exclude(id__in=user_hidden_drinks)
  no_user_drinks = all_drinks.exclude(id__in=all_user_drinks)

  try: f_ing = int(f_ing)
  except: 
      pass

  if isinstance(f_ing, int):
    filter_ingredient = all_ingredients.get(pk = f_ing)
    drinks = drinks.filter(ingredients=filter_ingredient)
    f_ing = 'With %s' % filter_ingredient
  elif f_ing == 'favorites':
    drinks = user_favorites_drinks 
  elif f_ing == 'hidden':
    drinks = user_hidden_drinks
  elif f_ing == 'possible-recommended':
    drinks = drinks.filter(top=True)
  elif f_ing == 'all-drinks':
    drinks = all_drinks
  else:
    drinks = all_drinks
  print (drinks)
  print ('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
  # return render(request, 'bar/user_drinks.html', {'drinks': drinks, 'user_hidden_drinks': user_hidden_drinks, 'user_ingredients': user_ingredients, 'user_favorites_drinks': user_favorites_drinks, 'no_user_ingredients': no_user_ingredients, 'no_user_drinks': no_user_drinks, 'f_ing': f_ing, 'user_top_ingredients': user_top_ingredients, 'user_no_top_ingredients': user_no_top_ingredients})
  return drinks
'''


'''
class HiddenUserList(ListView):
    template_name = 'bar/user_drinks.html'
    context_object_name = 'drinks'
    # queryset = request.user.profile.hidden_drinks.all()

    def get_queryset(self):
        # original qs
        # qs = super().get_queryset() 
        # filter by a variable captured from url, for example
        # return qs
        user = self.request.user
        return user.profile.hidden_drinks.all()


class FavoritesUserList(ListView):
  template_name = 'bar/user_drinks.html'

  def get_favorites(self):
    user = self.request.user
    return user.profile.favorite_drinks.all()

  def get_context_data(self, **kwargs):
    context = super().get_context_data(**kwargs)
    context['user_favorites_drinks'] = self.get_favorites()
    return context
'''

'''

def user_standard_data(request):
  user = request.user
  all_ingredients = Ingredient.objects.all()
  all_drinks = Drink.objects.all()
  # user_ingredients = all_ingredients.filter(users__id=user.id)
  user_ingredients = user.profile.ingredients.all().order_by('-top').order_by('hierarchy')

  return { 'user': user, 'all_ingredients': all_ingredients,  'all_drinks': all_drinks, 'user_ingredients': user_ingredients}


def user_data(function):
  @wraps(function)
  @allow_lazy_user
  def wrap(request, pk='', *args, **kwargs):
    user = request.user
    all_ingredients = Ingredient.objects.all()
    all_drinks = Drink.objects.all()
    user_ingredients = user.profile.ingredients.all().order_by('-top').order_by('hierarchy')
    user_hidden_drinks = user.profile.hidden_drinks.all()

    if pk:
      drinks = function(request, user_ingredients, user, all_drinks, all_ingredients, user_hidden_drinks, pk)
    else:
      drinks = function(request, user_ingredients, user, all_drinks, all_ingredients, user_hidden_drinks) 
    # content = {'drinks': drinks, 'user_hidden_drinks': user_hidden_drinks, 'user_ingredients': user_ingredients, 'user_favorites_drinks': user_favorites_drinks, 'no_user_ingredients': no_user_ingredients, 'no_user_drinks': no_user_drinks, 'f_ing': f_ing, 'user_top_ingredients': user_top_ingredients, 'user_no_top_ingredients': user_no_top_ingredients}
    content = {'drinks': drinks, 'user_hidden_drinks': user_hidden_drinks, 'user_ingredients': user_ingredients }

    return render(request, 'bar/user_drinks.html', content)
    
  return wrap


'''
