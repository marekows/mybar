from django.shortcuts import render, redirect, get_object_or_404
from bar.models import Ingredient
from django.http import HttpResponseRedirect
from lazysignup.decorators import allow_lazy_user
from django.contrib import messages
from django.http import JsonResponse


@allow_lazy_user
def list_all(request):

    user = request.user

    user_ingredients = user.profile.ingredients.all()
    all_ingredients = Ingredient.objects.all()
    
    ingredients_category_1 = [
        element for element in all_ingredients if element.hierarchy == 1]
    ingredients_category_2 = [
        element for element in all_ingredients if element.hierarchy == 2]
    ingredients_category_3 = [
        element for element in all_ingredients if element.hierarchy == 3]

    context = {'user_ingredients': user_ingredients,
               'ingredients_category_1': ingredients_category_1,
               'ingredients_category_2': ingredients_category_2,
               'ingredients_category_3': ingredients_category_3}

    return render(request, 'bar/ingredients.html', context)

'''
@allow_lazy_user
def add_ingredient(request, pk):
    user = request.user
    ingredient = get_object_or_404(Ingredient, pk=pk)
    user.profile.ingredients.add(ingredient)
    user.profile.is_calculated = False
    user.profile.save()

    messages.success(request, '%s - added' % ingredient.name.title())
    return redirect(list_all)


@allow_lazy_user
def remove_ingredient(request, pk):
    user = request.user
    ingredient = get_object_or_404(Ingredient, pk=pk)
    user.profile.ingredients.remove(ingredient)
    user.profile.is_calculated = False
    user.profile.save()

    messages.success(request, '%s - removed' % ingredient.name.title())
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
'''


@allow_lazy_user
def remove_ingredient(request, pk):
    if request.method == 'GET' and request.is_ajax():
        try:
            user = request.user
            ingredient = get_object_or_404(Ingredient, pk=pk)
            user.profile.ingredients.remove(ingredient)
            user.profile.is_calculated = False
            user.profile.save()
            return JsonResponse({'status': 'Success', 'message': 'Recoard has been deleted.', 'name': ingredient.name.title() })
        except Ingredient.DoesNotExist:
            return JsonResponse({'status': 'Fail', 'message': 'Recoard does not exist.'})
    return JsonResponse({'status': 'Fail', 'message': 'Error, must be an Ajax call.'})


@allow_lazy_user
def add_ingredient(request, pk):
    if request.method == 'GET' and request.is_ajax():
        try:
            user = request.user
            ingredient = get_object_or_404(Ingredient, pk=pk)
            user.profile.ingredients.add(ingredient)
            user.profile.is_calculated = False
            user.profile.save()
            return JsonResponse({'status': 'Success', 'message': 'Recoard has been deleted.', 'name': ingredient.name.title() })
        except Ingredient.DoesNotExist:
            return JsonResponse({'status': 'Fail', 'message': 'Recoard does not exist.'})
    return JsonResponse({'status': 'Fail', 'message': 'Error, must be an Ajax call.'})