from django.shortcuts import render, redirect
from lazysignup.decorators import allow_lazy_user
from .ingredients import list_all


def about(request):
    return render(request, 'bar/about.html')


@allow_lazy_user
def main_redirect(request):
    return redirect(list_all)
