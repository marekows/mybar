from django.shortcuts import render, get_object_or_404
from bar.models import Drink, Ingredient
from lazysignup.decorators import allow_lazy_user
from django.http import JsonResponse
from django.core.paginator import Paginator


@allow_lazy_user
def all_list(request):

    user = request.user
    all_drinks = Drink.objects.all()
    user_hidden_drinks = user.profile.hidden_drinks.all()
    user_favorites_drinks = user.profile.favorite_drinks.all()
    drinks = all_drinks.exclude(id__in=user_hidden_drinks)
    all_ingredients = Ingredient.objects.all()
    paginator = Paginator(drinks, 12)
    page = request.GET.get('page')
    drinks = paginator.get_page(page)
    
    title = 'All Drinks'
    context = {'title': title, 'drinks': drinks,
               'ingredients': all_ingredients,
               'user_favorites_drinks': user_favorites_drinks,
               'user_hidden_drinks': user_hidden_drinks}

    return render(request, 'bar/all_drinks.html', context)


@allow_lazy_user
def recommended_list(request):
    user = request.user
    all_ingredients = Ingredient.objects.all()
    drinks = Drink.objects.filter(top=True)
    user_hidden_drinks = user.profile.hidden_drinks.all()
    user_favorites_drinks = user.profile.favorite_drinks.all()
    paginator = Paginator(drinks, 12)
    page = request.GET.get('page')
    drinks = paginator.get_page(page)
    title = 'Recommended Drinks'

    context = {'title': title, 'drinks': drinks,
               'ingredients': all_ingredients,
               'user_favorites_drinks': user_favorites_drinks,
               'user_hidden_drinks': user_hidden_drinks}

    return render(request, 'bar/all_drinks.html', context)


@allow_lazy_user
def by_ingredient_list(request, pk):
    user = request.user
    all_ingredients = Ingredient.objects.all()
    user_hidden_drinks = user.profile.hidden_drinks.all()
    user_favorites_drinks = user.profile.favorite_drinks.all()
    filter_ingredient = all_ingredients.get(pk=pk)
    drinks = Drink.objects.all().exclude(
        id__in=user_hidden_drinks).filter(ingredients=filter_ingredient)
    title = 'Drinks with %s' % filter_ingredient.name.title()
    paginator = Paginator(drinks, 12)
    page = request.GET.get('page')
    drinks = paginator.get_page(page)
    context = {'title': title, 'drinks': drinks,
               'user_hidden_drinks': user_hidden_drinks,
               'user_favorites_drinks': user_favorites_drinks,
               'ingredients': all_ingredients}

    return render(request, 'bar/all_drinks.html', context)


@allow_lazy_user
def drink_details(request, pk):
    if request.method == 'GET' and request.is_ajax():
        try:
            user = request.user
            drink = get_object_or_404(Drink, pk=pk)
            hidden = user.profile.hidden_drinks.all()
            is_hidden = drink in hidden
            is_favorite = drink in user.profile.favorite_drinks.all()

            return JsonResponse({'status': 'Success', 'message': 'Recoard has been deleted.',
                                 'name': drink.name.title(), 'description': drink.full_recipe,
                                 'is_hidden': is_hidden, 'is_favorite': is_favorite})
        except Drink.DoesNotExist:
            return JsonResponse({'status': 'Fail', 'message': 'Recoard does not exist.'})
    return JsonResponse({'status': 'Fail', 'message': 'Error, must be an Ajax call.'})
