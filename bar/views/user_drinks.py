from django.shortcuts import render, get_object_or_404
from bar.models import Drink, Ingredient, Profile
from django.http import HttpResponseRedirect
from lazysignup.decorators import allow_lazy_user
from django.contrib import messages
from django.db.models import Avg, Count
from lazysignup.utils import is_lazy_user
from django.core.paginator import Paginator

from django.http import JsonResponse


@allow_lazy_user
def all_list(request):
    profile = request.user.profile
    profile_ingredients = profile.ingredients.all()
    profile_favorites_drinks = profile.favorite_drinks.all()

    drinks = calculate_drinks(profile, profile_ingredients)
    paginator = Paginator(drinks, 12)
    page = request.GET.get('page')
    drinks = paginator.get_page(page)
    title = 'All Possible Drinks'
    context = {'title': title, 'drinks': drinks,
               'user_ingredients': profile_ingredients,
               'user_favorites_drinks': profile_favorites_drinks}

    return render(request, 'bar/user_drinks.html', context)


@allow_lazy_user
def recommended_list(request):
    profile = request.user.profile
    profile_ingredients = profile.ingredients.all()

    drinks = calculate_drinks(profile, profile_ingredients)
    drinks = drinks.filter(top=True)
    paginator = Paginator(drinks, 12)
    page = request.GET.get('page')
    drinks = paginator.get_page(page)
    profile_favorites_drinks = profile.favorite_drinks.all()
    title = 'Recommended Possible Drinks'
    paginator = Paginator(drinks, 12)
    page = request.GET.get('page')
    drinks = paginator.get_page(page)
    context = {'title': title, 'drinks': drinks,
               'user_ingredients': profile_ingredients,
               'user_favorites_drinks': profile_favorites_drinks}

    return render(request, 'bar/user_drinks.html', context)


@allow_lazy_user
def favorites_list(request):
    profile = request.user.profile
    profile_favorites_drinks = profile.favorite_drinks.all()
    profile_ingredients = profile.ingredients.all()
    profile_hidden_drinks = profile.hidden_drinks.all()
    no_profile_ingredients = Ingredient.objects.exclude(
        id__in=profile_ingredients)
    no_profile_drinks = Drink.objects.filter(
        ingredients__in=no_profile_ingredients)
    title = 'Favorites Drinks'
    paginator = Paginator(profile_favorites_drinks, 12)
    page = request.GET.get('page')
    profile_favorites_drinks = paginator.get_page(page)
    context = {'title': title, 'drinks': profile_favorites_drinks,
               'user_favorites_drinks': profile_favorites_drinks,
               'user_hidden_drinks': profile_hidden_drinks,
               'no_user_drinks': no_profile_drinks,
               'no_user_ingredients': no_profile_ingredients,
               'user_ingredients': profile_ingredients}

    return render(request, 'bar/user_drinks.html', context)


@allow_lazy_user
def by_ingredient_list(request, pk):
    profile = request.user.profile
    profile_ingredients = profile.ingredients.all()
    profile_favorites_drinks = profile.favorite_drinks.all()
    filter_ingredient = Ingredient.objects.get(pk=pk)
    drinks = calculate_drinks(profile, profile_ingredients).filter(
        ingredients=filter_ingredient)
    title = 'Possible Drinks with %s' % filter_ingredient.name.title()
    paginator = Paginator(drinks, 12)
    page = request.GET.get('page')
    drinks = paginator.get_page(page)
    context = {'title': title, 'drinks': drinks,
               'user_ingredients': profile_ingredients,
               'user_favorites_drinks': profile_favorites_drinks}

    return render(request, 'bar/user_drinks.html', context)


@allow_lazy_user
def hidden_list(request):
    user = request.user
    user_ingredients = user.profile.ingredients.all()
    user_hidden_drinks = user.profile.hidden_drinks.all()
    title = 'Hidden Drinks'
    hidden = True
    paginator = Paginator(user_hidden_drinks, 12)
    page = request.GET.get('page')
    user_hidden_drinks = paginator.get_page(page)
    context = {'title': title, 'hidden': hidden,
               'drinks': user_hidden_drinks,
               'user_ingredients': user_ingredients}

    return render(request, 'bar/user_drinks.html', context)


def calculate_drinks(profile, user_ingredients=None, user_hidden_drinks=None):

    if profile.is_calculated:

        list_drinks = profile.possible_drinks
        drinks = Drink.objects.filter(
            id__in=list_drinks)
    else:
        if user_ingredients is None:
            user_ingredients = profile.ingredients.all()
        if user_hidden_drinks is None:
            user_hidden_drinks = profile.hidden_drinks.all()

        drinks = Drink.objects.exclude(
            ingredients__in=Ingredient.objects.exclude(id__in=user_ingredients)
        ).exclude(id__in=user_hidden_drinks)

        old_list_drinks = drinks.values_list('pk', flat=True)
        list_drinks = list(old_list_drinks)

        profile.possible_drinks = list_drinks
        profile.is_calculated = True
        profile.save()

    return drinks


@allow_lazy_user
def add_hidden_drink(request, pk, ):
    user = request.user
    drink = get_object_or_404(Drink, pk=pk)
    user.profile.hidden_drinks.add(drink)
    user.profile.is_calculated = False
    user.profile.save()

    messages.success(request, '%s - hidden' % drink.name.title())
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@allow_lazy_user
def remove_hidden_drink(request, pk):
    user = request.user
    drink = get_object_or_404(Drink, pk=pk)
    user.profile.hidden_drinks.remove(drink)
    user.profile.is_calculated = False
    user.profile.save()

    messages.success(request, '%s - unhidden' % drink.name.title())
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@allow_lazy_user
def add_favorite_drink(request, pk):
    user = request.user
    drink = get_object_or_404(Drink, pk=pk)
    user.profile.favorite_drinks.add(drink)
    user.profile.save()

    messages.success(request, '%s - added to favorites' % drink.name.title())
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@allow_lazy_user
def remove_favorite_drink(request, pk):
    user = request.user
    drink = get_object_or_404(Drink, pk=pk)
    user.profile.favorite_drinks.remove(drink)
    user.profile.save()
    messages.success(request, '%s removed from favorites' % drink.name.title())
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@allow_lazy_user
def switch_hidden_drink(request, pk, ):
    if request.method == 'GET' and request.is_ajax():
        try:
            user = request.user
            drink = get_object_or_404(Drink, pk=pk)
            user_hidden_drinks = user.profile.hidden_drinks.all()
            is_hidden = drink in user_hidden_drinks
            if is_hidden:
                user.profile.hidden_drinks.remove(drink)
            else:
                user.profile.hidden_drinks.add(drink)
            user.profile.is_calculated = False
            user.profile.save()

            return JsonResponse({'status': 'Success', 'message': 'xxxxxxxxxxxx',
                                 'is_hidden': not is_hidden})
        except Drink.DoesNotExist:
            return JsonResponse({'status': 'Fail', 'message': 'Recoard does not exist.'})

    return JsonResponse({'status': 'Fail', 'message': 'Error, must be an Ajax call.'})


@allow_lazy_user
def switch_favorite_drink(request, pk, ):
    if request.method == 'GET' and request.is_ajax():
        try:
            user = request.user
            drink = get_object_or_404(Drink, pk=pk)
            user_favorite_drinks = user.profile.favorite_drinks.all()
            is_favorite = drink in user_favorite_drinks
            if is_favorite:
                user.profile.favorite_drinks.remove(drink)
            else:
                user.profile.favorite_drinks.add(drink)
            user.profile.is_calculated = False
            user.profile.save()

            return JsonResponse({'status': 'Success', 'message': 'xxxxxxxxxxxx',
                                 'is_favorite': not is_favorite})
        except Drink.DoesNotExist:
            return JsonResponse({'status': 'Fail', 'message': 'Recoard does not exist.'})

    return JsonResponse({'status': 'Fail', 'message': 'Error, must be an Ajax call.'})
