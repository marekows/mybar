from django.test import TestCase
from .models import Ingredient


class AnimalTestCase(TestCase):
    def setUp(self):
        Ingredient.objects.create(name="lion", hierarchy=1, top='gfdg')

    def test_animals_can_speak(self):
        """Animals that can speak are correctly identified"""
        lion = Ingredient.objects.get(name="lion")
        self.assertEqual(lion.top, True)
