from django.test import RequestFactory
from django.contrib.sessions.middleware import SessionMiddleware
from django.contrib.messages.storage.fallback import FallbackStorage
from django.urls import reverse
from django.contrib.auth.models import User
from bar.views import about, ingredients, add_ingredient
from mixer.backend.django import mixer
import pytest


@pytest.mark.django_db
class TestViews():

    def test_show_about(self):
        path = reverse('about')
        request = RequestFactory().get(path)

        respone = about(request)
        assert respone.status_code == 200

    def test_show_ingredients(self):

        ingredient = mixer.blend('bar.Ingredient', name='kokshjk')
        ingredient2 = mixer.blend('bar.Ingredient', name='mastablasta')
        self.user = mixer.blend(User)

        self.user.profile.ingredients.add(ingredient)
        self.user.profile.ingredients.add(ingredient2)
        self.user.profile.save()
        print(self.user.profile.ingredients.all())
        path = reverse('ingredients')
        self.request = RequestFactory().get(path)

        self.request.user = self.user
        middleware = SessionMiddleware()
        middleware.process_request(self.request)
        response = ingredients(self.request)
        print(self.user)

        assert response.status_code == 200

    def test_add_ingredient_to_user(self):

        ingredient = mixer.blend('bar.Ingredient', name='koks')
        ingredient2 = mixer.blend('bar.Ingredient', name='mastablasta')
        user = mixer.blend(User)
        user.profile.ingredients.add(ingredient)
        user.profile.save()
        path = reverse('add_ingredient', kwargs={'pk': ingredient2.pk})
        request = RequestFactory().get(path)
        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.user = user
        request.session.save()
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        add_ingredient(request, pk=ingredient2.id)
        response = add_ingredient(request, pk=ingredient2.id)
        assert response.status_code == 302
        assert ingredient2 in request.user.profile.ingredients.all()