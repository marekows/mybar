from mixer.backend.django import mixer
from bar.models import Ingredient, Drink
from django.test import TestCase
import pytest


@pytest.mark.django_db
class TestIngredientModel(TestCase):

    def test_add_drink_with_ingredients(self):
        self.ingredients = mixer.cycle(10).blend(Ingredient)
        self.drink = mixer.blend(Drink)
        for _ in range(4):
            self.drink.ingredients.add(self.ingredients[_ * 2])
        self.drink.save()
        self.assertEqual(4, len(self.drink.ingredients.all()))

    def test_add_top_drink(self):
        self.top_drink = mixer.blend(Drink, top=True)
        self.no_top_drink = mixer.blend(Drink, top=False)
        drinks = Drink.objects.all()
        self.assertEqual(drinks[0].top, True)
        self.assertEqual(drinks[1].top, False)
