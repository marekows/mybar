from bar.models import Profile, User, Ingredient, Drink
from mixer.backend.django import mixer
from django.test import TestCase
import pytest


@pytest.mark.django_db
class TestProfileModel(TestCase):

    def setUp(self):
        self.user = mixer.blend(User)
        self.profile = self.user.profile

    def test_profile_added_with_user(self):
        self.assertEqual(self.profile, Profile.objects.all().latest('id'))

    def test_deleting_user_with_profile(self):
        self.user.delete()
        with self.assertRaises(Profile.DoesNotExist):
            Profile.objects.get(pk=self.user.id)

    def test_add_ingredient_to_profile(self):
        ingredient = mixer.blend(Ingredient)
        self.profile.ingredients.add(ingredient)
        self.profile.save()
        self.assertIn(ingredient, self.profile.ingredients.all())

    def test_add_hidden_drink_to_profile(self):
        drinks = mixer.cycle(10).blend(Drink)
        for _ in range(5):
            self.profile.hidden_drinks.add(drinks[_ * 2])
        self.profile.save()
        self.assertEqual(5, len(self.profile.hidden_drinks.all()))

    def test_add_favorite_drink_to_profile(self):
        drinks = mixer.cycle(10).blend(Drink)
        for _ in range(5):
            self.profile.favorite_drinks.add(drinks[_ * 2])
        self.profile.save()
        self.assertEqual(5, len(self.profile.favorite_drinks.all()))

    @pytest.mark.xfail
    def test_add_same_drink_as_favorite_and_hidden_to_profile(self):
        drink = mixer.blend(Drink)
        self.profile.favorite_drinks.add(drink)
        self.profile.save()
        self.profile.hidden_drinks.add(drink)
        self.profile.save()
        self.fail(
            msg="There is no logic in model to prevent: Profile has same drink as hidden and favorite drink")
