from mixer.backend.django import mixer
from bar.models import Ingredient
from django.test import TestCase
import pytest


@pytest.mark.django_db
class TestIngredientModel(TestCase):

    def test_add_ingredient(self):
        ingredient = mixer.blend('bar.Ingredient', name='koks')
        # assert ingredient.name == 'koks'
        assert Ingredient.objects.get(name='koks') == ingredient
        assert ingredient.top is False
        assert ingredient.hierarchy == 1

    def test_ingredient_hierarchy(self):
        ingredient = mixer.blend('bar.Ingredient', hierarchy=11)
        # assert ingredient.name == 'koks'
        assert ingredient in Ingredient.objects.filter(hierarchy=11)

    def test_cycle_mixer(self):
        self.ingredients = mixer.cycle(5).blend('bar.Ingredient')
        self.assertEqual(len(self.ingredients), 5)

    def test_group_by_hierarchy(self):
        self.ingredients = mixer.cycle(15).blend(
            'bar.Ingredient', hierarchy=mixer.RANDOM)
        grouped_ingrediets = Ingredient.objects.all()
        i_grouped_ingredients = iter(grouped_ingrediets)

        next(i_grouped_ingredients)
        for _ in range(14):
            lower = grouped_ingrediets[_].hierarchy
            higher = next(i_grouped_ingredients).hierarchy
            self.assertGreater(higher, lower)

    def test_group_by_top(self):
        self.ingredients = mixer.cycle(15).blend(
            'bar.Ingredient', top=mixer.RANDOM)
        grouped_ingrediets = Ingredient.objects.all()
        i_grouped_ingredients = iter(grouped_ingrediets)

        next(i_grouped_ingredients)
        for _ in range(14):
            current_ing = grouped_ingrediets[_].top
            next_ing = next(i_grouped_ingredients).top
            if current_ing != next_ing:
                self.assertEqual(next_ing, False)

    def test_group_by_top_then_hierarchy(self):
        self.ingredients = mixer.cycle(30).blend(
            'bar.Ingredient', hierarchy=mixer.sequence(1, 2, 3), top=mixer.RANDOM)
        grouped_ingrediets = Ingredient.objects.all()
        i_grouped_ingredients = iter(grouped_ingrediets)

        next(i_grouped_ingredients)
        for _ in range(29):
            current_ing = grouped_ingrediets[_]
            next_ing = next(i_grouped_ingredients)
            if current_ing.top is False:
                self.assertEqual(next_ing.top, False)
                self.assertGreaterEqual(
                    next_ing.hierarchy, current_ing.hierarchy)
            if current_ing.hierarchy > next_ing.hierarchy and current_ing.top == next_ing.top:
                pytest.fail()
                print("%s %s" % (current_ing.top, current_ing.hierarchy))
                print("%s %s" % (next_ing.top, next_ing.hierarchy))
