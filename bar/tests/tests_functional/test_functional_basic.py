from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from selenium import webdriver


class TestProjectListPage(StaticLiveServerTestCase):

    @classmethod
    def setup_class(cls):
        cls.browser = webdriver.Chrome(
            '/usr/lib/chromium-browser/chromedriver')
        # cls.browser.get(cls.live_server_url)

    @classmethod
    def teardown_class(cls):
        '''
        teardown any state that was previously setup with a
        call to setup_class.
        '''
        cls.browser.close()

    def setUp(self):
        self.browser.get(self.live_server_url)

    def test_1(self):
        elem_drinks = self.browser.find_element_by_xpath(
            "/html//a[@id='navbarDropdown']")
        elem_drinks2 = self.browser.find_element_by_link_text("Drinks")
        self.assertEqual(elem_drinks, elem_drinks2)

    def test_2(self):
        elem_drinks = self.browser.find_elements_by_xpath(
            "/html//a[@id='navbarDropdown']")
        self.assertEqual(len(elem_drinks), 2)
        print(elem_drinks)

    def test_to_about_page(self):
        about_url = self.live_server_url + reverse('about')
        self.browser.find_element_by_link_text("About").click()
        self.assertEqual(about_url, self.browser.current_url)
        # time.sleep(1)
        # pytest.fail(msg="abrakadabra")

    def test_to_my_drinks_page(self):
        about_url = self.live_server_url + reverse('user_drinks')
        self.browser.find_element_by_link_text("Drinks").click()
        self.browser.find_element_by_link_text("My Drinks").click()
        self.assertEqual(about_url, self.browser.current_url)

    def test_to_all_drinks_page(self):
        about_url = self.live_server_url + reverse('all_drinks')
        self.browser.find_element_by_link_text("Drinks").click()
        self.browser.find_element_by_link_text("All Drinks").click()
        self.assertEqual(about_url, self.browser.current_url)

    def test_register_form(self):
        self.browser.find_element_by_link_text("Account").click()
        self.browser.find_element_by_link_text("Register").click()
        print(self.browser.find_elements_by_xpath(
            "/html//input[@id='id_username']"))
        self.assertTrue(self.browser.find_elements_by_xpath(
            "/html//input[@id='id_username']"))
        self.assertTrue(self.browser.find_elements_by_xpath(
            "/html//input[@id='id_password1']"))

    '''
    @classmethod
    def firstsetUp(cls):
            super(TestProjectListPage, cls).firstsetUp
            cls.browser = webdriver.Chrome(
                '/usr/lib/chromium-browser/chromedriver')

    def __init__(self, *args, **kwargs):
            super(TestProjectListPage, self).__init__(*args, **kwargs)
            # TestProjectListPage.__init__(self)
            self.browser = webdriver.Chrome(
                '/usr/lib/chromium-browser/chromedriver')

    @classmethod
    def setUp(self):
            super(TestProjectListPage, cls).setUp
            cls.browser.get(self.live_server_url)
            cls.browser = webdriver.Chrome(
                '/usr/lib/chromium-browser/chromedriver')
            # self.browser.get(self.live_server_url)
    @classmethod
    def tearDown(cls):
            super(TestProjectListPage, cls).tearDown
            cls.browser.close()
            # self.browser.get(self.live_server_url)
    '''
