from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import pytest
from mixer.backend.django import mixer
import time
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


class TestProjectListPage(StaticLiveServerTestCase):

    @classmethod
    def setup_class(cls):
        cls.browser = webdriver.Chrome(
            '/usr/lib/chromium-browser/chromedriver')

    @classmethod
    def teardown_class(cls):
        '''
        teardown any state that was previously setup with a
        call to setup_class.
        '''
        cls.browser.close()

    @pytest.mark.django_db
    def setUp(self):
        self.ingredients = mixer.cycle(10).blend('bar.Ingredient')
        self.browser.get(self.live_server_url)

    def test_adding_ingredient(self):
        elem_drinks = self.browser.find_elements_by_xpath(
            "/html//a[@id='navbarDropdown']")
        self.assertEqual(len(elem_drinks), 2)
        print(elem_drinks)

    def test_veryfing_msg_after_adding_ingredient(self):
        '''
        Veryfing if MSG appers after adding ingredient for 1.5s (set as 1.8s)
        consider to checking it in other way, using:
        - div[class*='fadeshow']:not([style='display: none;'])
        or
        - style="outline: orange dashed 2px !important;
        '''
        self.browser.find_element_by_link_text(
            self.ingredients[1].name).click()
        for _ in range(3):
            try:
                time.sleep(0.5)
                self.browser.find_element_by_css_selector(
                    "div[style='display: none;']")
                pytest.fail(msg="No MSG or to fast hidden")
            except NoSuchElementException:
                pass
        time.sleep(1)
        try:
            self.browser.find_element_by_css_selector(
                "div[style='display: none;']")
        except NoSuchElementException:
            pytest.fail(msg="Msg didn't hidden")
