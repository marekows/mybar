from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver


class TestRegisterForm(StaticLiveServerTestCase):

    @classmethod
    def setup_class(cls):
        cls.browser = webdriver.Chrome(
            '/usr/lib/chromium-browser/chromedriver')

    @classmethod
    def teardown_class(cls):
        '''
        teardown any state that was previously setup with a
        call to setup_class.
        '''
        cls.browser.close()

    def setUp(self):
        self.browser.get(self.live_server_url)

    def test_required_fields_exist(self):
        fields_input_id = ['id_first_name', 'id_last_name',
                           'id_password', 'id_email_address',
                           'id_date_of_birth']
        self.browser.find_element_by_link_text("Register").click()

        for input_id in fields_input_id:
            self.assertTrue(self.browser.find_elements_by_xpath(
                "/html//input[@id='id_%s']" % input_id))

    # Place for next tests in this class