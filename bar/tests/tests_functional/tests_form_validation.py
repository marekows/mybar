from django.contrib.auth.forms import UserCreationForm
from django.test import TestCase


class TestForms(TestCase):

    def test_empty_fields_case_invalid(self):
        form = UserCreationForm(
            data={'username': '', 'password1': '', 'password2': ''})
        for field in form.fields:
            self.assertIn(field, form.errors)

    def test_all_fields_provisioned_case_valid(self):
        form = UserCreationForm(
            data={'username': 'testname', 'password1': 'testpassword', 'password2': 'testpassword'})

        self.assertTrue(form.is_valid())

    def test_password_validation_case_invalid(self):
        invalid_passwords = ['invalidPassword', 'invalid8password', 'I8Pass']
        for password in invalid_passwords:
            form = UserCreationForm(
                data={'password1': password, 'password2': password})

            self.assertIn('username', form.errors)

    def test_password_validation_case_valid(self):
        valid_passwords = ['Valid8Password', '2Valid8Password']
        for password in valid_passwords:
            form = UserCreationForm(
                data={'password1': password, 'password2': password})

            self.assertNotIn('password1', form.errors)
