from django.urls import reverse, resolve

class TestUrls:

    def test_detail_url_1(self):
        path = reverse('user_drinks_by_ingredient', kwargs={'pk': 1})
        assert resolve(path).view_name == 'user_drinks_by_ingredient'

    def test_detail_url_2(self):
        path = reverse('add_hidden_drink', kwargs={'pk': 1})
        assert resolve(path).view_name == 'add_hidden_drink'
