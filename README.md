# My Bar    
  
#### In "My Bar" you pick all your bar equipment and it shows you all possible drinks which you can prepare !!!   
#### Sounds awesome yeah?  
#### Pick some drinks which looks for you really tasty, add them to your favorites. If you miss some ingredients, "My Bar" will let you know. 

### Check "My Bar" online: 
## [My Bar on AWS Elastic Beanstalk](http://mybar-dev.us-west-2.elasticbeanstalk.com/)


## Getting Started

"My Bar" based on Django Web Framework. For frontedend bootstrap is used with additional javascript for message notification. Tests are written in Python using Selenium, UnitTest and Pytest library.  
Most fancy modules used:   
1. django-lazysignup  
2. django-lazysignup (still need to be finnal implemented)  

### Prerequisites

All library needed you can find in `requirements.txt` in main project directory.
For local usage PostgreSQL is choosen as DB. If this is a problem for you just change DB in `/mysite/settings.py` file.

### Installing

..............

### Running the tests

.................


## Deployment

..........................


## Authors

* **Marek Owsianowski** - *Initial work* - [LinkedIn](https://www.linkedin.com/in/marek-owsianowski-b04642a8/)

