from django.test import TestCase
from django.contrib.auth.forms import UserCreationForm
from django.test import SimpleTestCase
from forms import UserUpdateForm
import json


class TestForms(TestCase):

    def test_form_validation(self):
        form = UserUpdateForm(data={'username': 'marek', 'email': ''})
        for field in form:
            print(field.errors)
        for error in form.errors:
            print(error)

        data = form.errors.as_json()
        data = json.loads(data)
        for error in data['email']:
            print(error['code'])

        self.assertIn('This field is required.', form.errors['email'])
        self.assertIn('required.', form.errors.as_json())
        # self.assertTrue(form.is_valid())
# Create your tests here.
