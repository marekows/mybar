from django.urls import path
from . import views

urlpatterns = [
    path('index/', views.index, name='index'),
    path('register', views.register, name='register'),
    path('update', views.user_update, name='user_update'),

]
