from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from bar.models import Drink, Ingredient, User
from django.http import HttpResponse, HttpResponseRedirect
from .forms import UserUpdateForm


from django.http import HttpResponse
from lazysignup.decorators import allow_lazy_user


# Create your views here.

@allow_lazy_user
def index(request):
	user = request.user
	return render(request, 'userapp/index.html')

@allow_lazy_user
def register(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)

		if form.is_valid():
			form.save()
			username = form.cleaned_data['username']
			password = form.cleaned_data['password1']
			user = authenticate(username=username, password=password)
			login(request, user)
			return redirect('/')
	else:
		form = UserCreationForm()

	context = {'form' : form}
	return render(request, 'registration/register.html', context)

@login_required
def user_update(request):
	if request.method == 'POST':
		form = UserUpdateForm(request.POST, instance=request.user)

		if form.is_valid():
			form.save()
			messages.success(request, f'Updated')
			return redirect('/')
	else:
		form = UserUpdateForm(instance=request.user)

	context = { 'form': form }
	return render(request, 'userapp/user_update.html', context)


