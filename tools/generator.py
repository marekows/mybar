import json

#jsonFile = open('ingredients.json', 'w')

#ingredients = ['vodka', 'dark rum', 'tequilla', 'triple sec', 'gin', 'orange juice', 'grenadine']
ingredients = ['grenadine', 'lemon juice', 'lime juice', 'melon liqure', 'whisky', 'bourbon', 'sweet vermouth']
with open('data.json', 'w') as outfile:  
	outfile.write('[\n')	
	for ingredient in ingredients:
		data = {}  
		data['model'] = 'bar.ingredient' 
		fields = {}
		fields['name'] = ingredient
		data['fields'] = fields

		
		json.dump(data, outfile)
		if ingredient != ingredients[-1]:
			outfile.write(',\n') 
	outfile.write('\n]')	


